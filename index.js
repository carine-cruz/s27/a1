const HTTP = require(`http`);
const myPort = 4000;

HTTP.createServer((req,res)=>{

    // console.log(`connected`);
    switch(req.url){
        case `/`: 
            res.writeHead(200, {"Content-type": "text/plain"});
            res.end(`Welcome to Booking System`);
            break;
        case `/profile`: 
            res.writeHead(200, {"Content-type": "text/plain"});
            res.end(`Welcome to your profile!`);
            break;
        case `/courses`:
            res.writeHead(200, {"Content-type": "text/plain"});
            res.end(`Here's our courses available`);
            break;
        case `/addcourse`:
            res.writeHead(200, {"Content-type": "text/plain"});
            res.end(`Add a course to our resources`);
            break;
        case `/updatecourse`:
            res.writeHead(200, {"Content-type": "text/plain"});
            res.end(`Update a course to our resources`);
            break;
        case `/archivecourse`:
            res.writeHead(200, {"Content-type": "text/plain"});
            res.end(`Archive courses to our resources`);
            break;
        default: 
            res.writeHead(404, {"Content-type":"plain/text"});
            res.write(`Page not found`);
            res.end(); 
    }

    // if (req.url === `/`){
    //     res.writeHead(200, {"Content-type": "text/plain"});
    //     res.end(`Welcome to Booking System`);
    // } else if (req.url === `/profile`){
    //     console.log(`Welcome to your profile!`);
    //     res.writeHead(200, {"Content-type": "text/plain"});
    //     res.end(`Welcome to your profile!`);
    // } else if (req.url === `/courses`){
    //     res.writeHead(200, {"Content-type": "text/plain"});
    //     res.end(`Here's our courses available`);
    // } else if (req.url === `/addcourse`){
    //     res.writeHead(200, {"Content-type": "text/plain"});
    //     res.end(`Add a course to our resources`);
    // } else if (req.url === `/updatecourse`){
    //     res.writeHead(200, {"Content-type": "text/plain"});
    //     res.end(`Update a course to our resources`);
    // } else if (req.url === `/archivecourses`){
    //     res.writeHead(200, {"Content-type": "text/plain"});
    //     res.end(`Archive courses to our resources`);
    // } else {
    //     res.writeHead(404, {"Content-type":"plain/text"});
    //     res.write(`Page not found`);
    //     res.end();
    // }


}).listen(myPort,()=> console.log(`Server connected to port ${myPort}`));